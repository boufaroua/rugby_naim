#include <iostream> 
#include <string> 
#include <iomanip>
#include "fctAffiche.h" 
using namespace std;
 
void afficheLigneVide(int tailleLigne) {
    if (tailleLigne <= 0) {
        cout << "La taille de la ligne doit être  positive." << endl;
        return;
    }

    for (int i = 0; i < tailleLigne; i++) {
        cout << "-";
    }
    cout << endl;
}

void afficheNbCar(char c, int nb) {
    for (int i = 0; i < nb; ++i) {
        cout << c;
    }
    
}

void afficheLigneComplete(int tailleLigne) {
    if (tailleLigne >= 2) {
        cout << "+";
        for (int i = 0; i < tailleLigne - 2; ++i) {
            cout << "-";
        }
        cout << "+\n";
    } else if (tailleLigne == 1) {
        cout << "+\n";
    } else {
        cout << "erreur taille";
    }
}

void afficheMenu(int tailleLigne) {
    cout << "| Appuyer sur :   |                   |\n";
    cout << "| 1 : Essai       | 7 : Essai         |\n";
    cout << "| 2 : Transf.     | 8 : Transf.       |\n";
    cout << "| 3 : Pen. ou Drop| 9 : Pen. ou Drop  |\n";
    cout << "| 4 : Essai de Pen| 6 : Essai de Pen  |\n";
    afficheLigneComplete(tailleLigne);
    cout << "| 5 : ANNULATION DE LA DERNIERE SAISIE|\n";
    afficheLigneComplete(tailleLigne);
    cout << "|  0 : Pour Quitter                   |\n";
    afficheLigneComplete(tailleLigne);
}

void afficheScore(int ptsEquipeA, int ptsEquipeB) {
    cout << "|      " << std::setw(2) << ptsEquipeA << "          |         " << std::setw(2) << ptsEquipeB << "       |\n";
}

void afficheLigneScores(int pPointA, int pPointB, int tailleLigne) {
    if (tailleLigne < 2) {
        cout << "entrer taille equipe > 2." << endl;
        return;
    }

    string scoreLine = "" + to_string(pPointA) + " | " + to_string(pPointB) + "";
    int espaceAvantScores = (tailleLigne - scoreLine.length()) / 2;

    cout << "|";
    for (int i = 0; i < espaceAvantScores; i++) {
        cout << " ";
    }
    cout << scoreLine;
    for (int i = 0; i < tailleLigne - espaceAvantScores - scoreLine.length(); i++) {
        cout << " ";
    }
    cout << "|" << endl;
}

void afficheNomEquipe(const std::string& pNomEquipeA, const std::string& pNomEquipeB, int tailleLigne) {
    if (tailleLigne < 5) {
        cout << "La taille de la ligne est trop petite " << endl;
        return;
    }
    int largeurNomEquipe = (tailleLigne - 3) / 2; 
    if (largeurNomEquipe < pNomEquipeA.length() || largeurNomEquipe < pNomEquipeB.length()) {
        cout << "La taille de la ligne est trop petite" << endl;
        return;
    }
    cout << "|";
    int espacesAvantEquipeA = (largeurNomEquipe - pNomEquipeA.length()) / 2;
    int espacesAvantEquipeB = (largeurNomEquipe - pNomEquipeB.length()) / 2;

    for (int i = 0; i < espacesAvantEquipeA; i++) {
        cout << " ";
    }
    cout << pNomEquipeA;

    for (int i = 0; i < largeurNomEquipe - espacesAvantEquipeA - pNomEquipeA.length(); i++) {
        cout << " ";
    }
    cout << "|";

    for (int i = 0; i < espacesAvantEquipeB; i++) {
        cout << " ";
    }
    cout << pNomEquipeB;

    for (int i = 0; i < largeurNomEquipe - espacesAvantEquipeB - pNomEquipeB.length(); i++) {
        cout << " ";
    }
    cout << "|" << endl;
}

void affichePanneau(std::string pNomA, std::string pNomB, int pPtsA, int pPtsB, int tailleLigne){
    afficheLigneComplete(tailleLigne);
    afficheNomEquipe(pNomA,pNomB, tailleLigne);
    afficheLigneComplete(tailleLigne);
    afficheScore(pPtsA, pPtsB);
    afficheLigneComplete(tailleLigne);
    afficheMenu(tailleLigne);
}

