#include <iostream>
#include <string>
#ifndef FCTAFFICHE_H
#define FCTAFFICHE_H

void afficheMenu(int tailleLigne);
void afficheScore(int ptsEquipeA, int ptsEquipeB);
void affichePanneau(std::string pNomA, std::string pNomB, int pPtsA, int pPtsB, int tailleLigne);
void afficheNbCar(char c, int nb);
void afficheLigneComplete(int tailleLigne);
void afficheLigneVide(int tailleLigne);
void afficheLigneScores(int pPointA, int pPointB, int tailleLigne);
void afficheNomEquipe(const std::string& pNomEquipeA, const std::string& pNomEquipeB, int tailleLigne);
#endif
